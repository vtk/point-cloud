set(DOCUMENTATION "This module contains point cloud filtering extensions to VTK.")

vtk_module( vtkPointCloud
  DESCRIPTION
    "${DOCUMENTATION}"
  DEPENDS
    vtkCommonDataModel
    vtkCommonExecutionModel
    vtkFiltersPoints
  TEST_DEPENDS
    vtkTestingCore
  KIT
    vtkRemote
)
