/*=========================================================================

  Program:   Visualization Toolkit
  Module:    vtkExtractPointCloudPiece.h

  Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
  All rights reserved.
  See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
// .NAME vtkExtractPointCloudPiece - Return a piece of a point cloud
// .SECTION Description
// This filter takes the output of a vtkHierarchicalBinningFilter and allows
// the pipeline to stream it. Pieces are detemined from an offset integral
// array is associated with the field data of the input.

#ifndef vtkExtractPointCloudPiece_h
#define vtkExtractPointCloudPiece_h

#include "vtkPointCloudModule.h" // For export macro
#include "vtkPolyDataAlgorithm.h"

class vtkIdList;
class vtkIntArray;

class VTKPOINTCLOUD_EXPORT vtkExtractPointCloudPiece : public vtkPolyDataAlgorithm
{
public:
  // Description:
  // Standard methods for instantiation, printing, and type information.
  static vtkExtractPointCloudPiece *New();
  vtkTypeMacro(vtkExtractPointCloudPiece, vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Turn on or off modulo sampling of the points. By default this is on and the
  // points in a given piece will be reordered in an attempt to reduce spatial
  // coherency.
  vtkSetMacro(ModuloOrdering,bool);
  vtkGetMacro(ModuloOrdering,bool);
  vtkBooleanMacro(ModuloOrdering,bool);

protected:
  vtkExtractPointCloudPiece();
  ~vtkExtractPointCloudPiece() {}

  // Usual data generation method
  int RequestData(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  int RequestUpdateExtent(vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  bool ModuloOrdering;

private:
  vtkExtractPointCloudPiece(const vtkExtractPointCloudPiece&);  // Not implemented.
  void operator=(const vtkExtractPointCloudPiece&);  // Not implemented.
};

#endif
